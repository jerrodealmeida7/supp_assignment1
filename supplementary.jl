### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ b8e167de-de4e-11eb-1854-817c0fb21cce
using Pkg

# ╔═╡ 6cb66b0a-1472-4843-b37a-2564b073aac5
md""" ### Problem 1. """

# ╔═╡ 9b3eefef-649f-4f47-8ab6-8bfcfbf2b3b0
mutable struct Individual
    chromosome
    fitness::Float32
    function Individual(size::Int, fitness)
        this = new()
        this.chromosome = rand(size) * absMax*2 - absMax
        this.fitness = fitness(this)
        return this
    end
    function Individual(data)
        this = new()
        this.chromosome = data
        this.fitness = 0.0
        return this
    end
end

# ╔═╡ 205125ed-5bfe-4b3c-a5e6-0d0008fa9236
Individual(9)

# ╔═╡ 9cfa21e8-ce3c-46b9-a7cc-c1165f0e5ed2
fitFunction = function(treeOutput)
	sqrt(mean((treeOutput)))
	fitness = sum(chromosome .* chromosome)
end

# ╔═╡ 0820445d-77b6-421b-8f50-52cdb646856e


# ╔═╡ 7235359f-3953-4f8f-b189-b1bff715b45a
function crossover(parent1::Individual, parent2::Individual)
    xover_chance = XR
    if rand() < xover_chance
        newChromosome = zeros(Float32, length(parent1.chromosome))
        for i = 1 : length(newChromosome)
            @inbounds newChromosome[i] = (rand() < 0.5) ?  parent1.chromosome[i] : parent2.chromosome[i]
        end
        return Individual(newChromosome)
    else
        return parent1
    end
end

# ╔═╡ e304cbc9-1df3-45b8-8b87-7d5aad6ecc84


# ╔═╡ 42e4ef96-0b50-4848-b4c5-2ec0b646601a
function mutate!(input::Individual)
    mut_amp = MA
    mut_rate = MR
    # mutator = () -> rand(Float32) < mut_rate ? rand(Float32)*mut_amp*2 - mut_amp : 0
    # input.chromosome .+= mutator.()
    for i = 1:length(input.chromosome)
        input.chromosome[i] += rand(Float32) < mut_rate ? rand(Float32)*mut_amp*2 - mut_amp : 0
    end
end


# ╔═╡ 8df69adb-d37d-4a29-9efa-283db350275a
md""" ### Problem 2. """

# ╔═╡ 8b3a58c3-0cd4-4961-b3db-757bbe834c07
struct ConstantFunctionDict{V}
    value::V

    function ConstantFunctionDict{V}(val::V) where V
        return new(val);
    end
end

# ╔═╡ 7a42e773-5c2e-414d-91a2-aed31f6ded3f
function backtracking_search(select_unassigned_variable, order_domain_values)
    local result = backtrack(problem, Dict(),
                            select_unassigned_variable=select_unassigned_variable,
                                    order_domain_values=order_domain_values);
    if (!(typeof(result) <: Nothing || goal_test(problem)))
        error("Unexpected result!")
    end
    return result;
end

# ╔═╡ 6fba77ba-fa19-421a-b048-5cb832191e7d
function forward_checking(T, var, value, assignment::Dict)
    for B in problem[var]
        if (!haskey(assignment, B))
            for b in copy(problem.current_domains[B])
                if (!problem.constraints(var, value, B, b))
                    prune(problem, B, b, removals);
                end
            end
            if (length(problem.current_domains[B]) == 0)
                return false;
            end
        end
    end
    return true;
	end

# ╔═╡ cc241a18-88e3-4add-a7fa-ba95a68f7170
function display(NQueensCSP, assignment::Dict)
    local num_of_vars::Int64 = length(problem.vars);
    for val in 1:num_of_vars
        for key in 1:num_of_vars
            local piece::String;
            if (get(assignment, key, "") == val)
                piece = "Q";
            elseif ((key + val - 1) % 2 == 0)
                piece = ".";
            end
            print(piece);
        end

        for key in 1:num_of_vars
            local piece::String;
            if (get(assignment, key, "") == val)
                piece = "*";
            else
                piece = " ";
            end
            print(nconflicts(problem, key, val, assignment), piece);
        end
	end
end

# ╔═╡ Cell order:
# ╠═6cb66b0a-1472-4843-b37a-2564b073aac5
# ╠═b8e167de-de4e-11eb-1854-817c0fb21cce
# ╠═9b3eefef-649f-4f47-8ab6-8bfcfbf2b3b0
# ╠═205125ed-5bfe-4b3c-a5e6-0d0008fa9236
# ╠═9cfa21e8-ce3c-46b9-a7cc-c1165f0e5ed2
# ╠═0820445d-77b6-421b-8f50-52cdb646856e
# ╠═7235359f-3953-4f8f-b189-b1bff715b45a
# ╠═e304cbc9-1df3-45b8-8b87-7d5aad6ecc84
# ╠═42e4ef96-0b50-4848-b4c5-2ec0b646601a
# ╠═8df69adb-d37d-4a29-9efa-283db350275a
# ╠═8b3a58c3-0cd4-4961-b3db-757bbe834c07
# ╠═7a42e773-5c2e-414d-91a2-aed31f6ded3f
# ╠═6fba77ba-fa19-421a-b048-5cb832191e7d
# ╠═cc241a18-88e3-4add-a7fa-ba95a68f7170
